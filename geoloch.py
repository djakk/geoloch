#!/usr/bin/python
# coding: utf8


import csv

import sys

import osmium

WKT_FAB = osmium.geom.WKTFactory()


# read the csv input file :
# header of the csv =  "Date ajout jj/mm/aa",Prenom,CP,Ville,Pays
the_csv_file = sys.stdin.read().splitlines()
the_members = [d for d in csv.DictReader(the_csv_file, delimiter=',')] # list of dicts with the keys = the header of the csv

NAME_HEADER, POSTAL_CODE_HEADER, CITY_HEADER = "Prenom", "CP", "Ville"


# for each row that is a Member (Membre), get the location of (postal code + town) (CP + Ville) with openstreetmap


class PlacesAsNodesHandler(osmium.SimpleHandler):
    def __init__(self):
        osmium.SimpleHandler.__init__(self)
        self.the_node_tags = []
    
    def node(self, n):
        the_tags_as_dict = {a_key.k : a_key.v for a_key in n.tags}
        the_tags_as_dict["OSM_ID"] = n.id
        the_tags_as_dict["longitude"] = n.location.lon
        the_tags_as_dict["latitude"] = n.location.lat
        self.the_node_tags.append(the_tags_as_dict)
        return


class RelationAsAreasHandler(osmium.SimpleHandler):
    def __init__(self):
        osmium.SimpleHandler.__init__(self)
        self.the_relation_tags = []
    
    #def relation(self, the_relation):
    #    for a_member in the_relation.members:
    #        if a_member.role == "admin_center"
    
    def area(self, a):
        the_tags_as_dict = {a_key.k : a_key.v for a_key in a.tags}
        the_tags_as_dict["OSM_ID"] = a.id
        try:
            the_tags_as_dict["WKT"] = WKT_FAB.create_multipolygon(a)
        except RuntimeError: # invalid geometry
            pass
        self.the_relation_tags.append(the_tags_as_dict)
        return

    


def normalize_a_text(the_text):
    """
    lowercase
    """
    if not bool(the_text):
        return the_text
    
    the_normalized_text = the_text.lower()
    
    the_normalized_text = the_normalized_text.replace("0", "O")
    
    the_normalized_text = the_normalized_text.replace("à", "a")
    the_normalized_text = the_normalized_text.replace("â", "a")
    the_normalized_text = the_normalized_text.replace("ä", "a")
    
    the_normalized_text = the_normalized_text.replace("é", "e")
    the_normalized_text = the_normalized_text.replace("è", "e")
    the_normalized_text = the_normalized_text.replace("ê", "e")
    the_normalized_text = the_normalized_text.replace("ë", "e")
    
    the_normalized_text = the_normalized_text.replace("î", "i")
    the_normalized_text = the_normalized_text.replace("ï", "i")
    
    the_normalized_text = the_normalized_text.replace("ô", "o")
    the_normalized_text = the_normalized_text.replace("ö", "o")
    
    the_normalized_text = the_normalized_text.replace("ù", "u")
    the_normalized_text = the_normalized_text.replace("û", "u")
    the_normalized_text = the_normalized_text.replace("ü", "u")
    
    the_normalized_text = the_normalized_text.replace("œ", "oe")
    
    the_normalized_text = the_normalized_text.replace("ç", "c")
    
    the_normalized_text = the_normalized_text.replace("-", " ")
    the_normalized_text = the_normalized_text.replace("/", " ")
    the_normalized_text = the_normalized_text.replace("'", " ")
    the_normalized_text = the_normalized_text.replace(",", " ")
    the_normalized_text = the_normalized_text.replace("(", " ")
    the_normalized_text = the_normalized_text.replace(")", " ")
    the_normalized_text = the_normalized_text.replace("_", " ")
    the_normalized_text = " ".join(the_normalized_text.split()) # "     " -> " "
    
    return the_normalized_text

def normalize_a_postcode(the_postcode):
    """
    string, not int
    """
    if not bool(the_postcode):
        return the_postcode
    
    the_postcode_normalized = the_postcode.lower()
    the_postcode_normalized = the_postcode_normalized.replace(" ", "")
    try:
        the_postcode_normalized = int(the_postcode_normalized) # "06500" -> "6500"
    except ValueError:
        pass
    else:
        the_postcode_normalized = str(the_postcode_normalized)
    return the_postcode_normalized

def levenshtein_distance(s, t, the_allowed_number_of_mistakes):
        """
        https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Python
        From Wikipedia article; Iterative with two matrix rows.
        
        # Christopher P. Matthews
        # christophermatthews1985@gmail.com
        # Sacramento, CA, USA
        """
        if s == t: return 0
        elif len(s) == 0: return len(t)
        elif len(t) == 0: return len(s)
        elif abs(len(t) - len(s)) > the_allowed_number_of_mistakes: return the_allowed_number_of_mistakes +1 # shortcut, the user can do 3 mistakes max in the input text
        v0 = [None] * (len(t) + 1)
        v1 = [None] * (len(t) + 1)
        for i in range(len(v0)):
            v0[i] = i
        for i in range(len(s)):
            v1[0] = i + 1
            for j in range(len(t)):
                cost = 0 if s[i] == t[j] else 1
                v1[j + 1] = min(v1[j] + 1, v0[j + 1] + 1, v0[j] + cost)
            for j in range(len(v0)):
                v0[j] = v1[j]
        
        return v1[len(t)]


    
h3 = PlacesAsNodesHandler()
h3.apply_file("/Volumes/Data/JJ/places.osm.pbf", locations=True)


h4 = RelationAsAreasHandler()
h4.apply_file("/Volumes/Data/JJ/zipcodes_areas.osm.pbf", locations=True)


the_total_number = len(h4.the_relation_tags)
the_counter = 0
the_relations_by_postcode = {}
for r in h4.the_relation_tags:
    the_counter += 1
    if the_counter % 500000 == 0:
        print(the_counter, "/", the_total_number, file=sys.stderr)
    the_postcode = normalize_a_postcode(r.get("addr:postcode", ""))
    try:
        the_relations_by_postcode[the_postcode].append(r)
    except KeyError:
        the_relations_by_postcode[the_postcode] = [r]


# building the_cities_of_osm : with already normalized names and postcodes, { frozendict( postcode ) : { frozendict( name ) : [  osm tags of city1,  osm tags of city2 … ]  } }
the_cities_osm__by_osm_id = {} # to avoid duplicated info ; nodes only
for a_node_as_tags in h3.the_node_tags:
    the_osm_id = a_node_as_tags["OSM_ID"]
    the_cities_osm__by_osm_id[the_osm_id] = a_node_as_tags
    # add extra info : the postcodes
    try:
        the_postcodes = a_node_as_tags.get('addr:postcode', a_node_as_tags.get('postcode', a_node_as_tags.get('postal_code'))).split(";")
    except KeyError:
        the_postcodes = []
    except AttributeError: # AttributeError: 'NoneType' object has no attribute 'split'
        the_postcodes = []
    the_postcodes_normalized = [ normalize_a_postcode(a_postcode) for a_postcode in the_postcodes ]
    the_cities_osm__by_osm_id[the_osm_id]["THE_POSTCODES"] = the_postcodes_normalized
    # add extra info : the names
    the_names = []
    for a_name_title in ("name", "alt_name", "name:fr", "name:en", "int_name", "short_name", "reg_name", "loc_name"):
        try:
            the_name = a_node_as_tags[a_name_title]
        except KeyError:
            pass
        else:
            the_names.extend( the_name.split(";") )
    the_names_normalized = [ normalize_a_text(a_name) for a_name in the_names ]
    the_cities_osm__by_osm_id[the_osm_id]["THE_NAMES"] = the_names_normalized


# get the corpus = get the normalized words of the differents names of the cities of the world
the_cities_as_osm_ids__by_normalized_words = {}
for a_city_as_osm_id, a_city in the_cities_osm__by_osm_id.items():
    for a_name in a_city["THE_NAMES"]:
        for a_word in a_name.split(): # the only separator bewteen words is space, thanks to normalize_a_text()
            try:
                the_cities_as_osm_ids__by_normalized_words[a_word].add(a_city_as_osm_id)
            except KeyError:
                the_cities_as_osm_ids__by_normalized_words[a_word] = set((a_city_as_osm_id,))

# the cities by name
the_cities_as_osm_ids__by_normalized_name = {}
for a_city_as_osm_id, a_city in the_cities_osm__by_osm_id.items():
    for a_name in a_city["THE_NAMES"]:
        try:
            the_cities_as_osm_ids__by_normalized_name[a_name].add(a_city_as_osm_id)
        except KeyError:
            the_cities_as_osm_ids__by_normalized_name[a_name] = set((a_city_as_osm_id,))


# get the postcodes corpus + the cities from osm without postcode
the_cities_as_osm_ids__by_normalized_postcode = {}
the_cities_as_osm_ids__by_normalized_postcode[""] = set()
for a_city_as_osm_id, a_city in the_cities_osm__by_osm_id.items():
    if not bool(a_city["THE_POSTCODES"]):
        the_cities_as_osm_ids__by_normalized_postcode[""].add(a_city_as_osm_id)
    for a_postcode in a_city["THE_POSTCODES"]:
        try:
            the_cities_as_osm_ids__by_normalized_postcode[a_postcode].add(a_city_as_osm_id)
        except KeyError:
            the_cities_as_osm_ids__by_normalized_postcode[a_postcode] = set((a_city_as_osm_id,))


the_meaningless_words = set()
for a_word, some_cities in the_cities_as_osm_ids__by_normalized_words.items():
    if len(some_cities) > 100:
        the_meaningless_words.add(a_word)



for a_member in sorted(the_members, key = lambda a_member : normalize_a_text(a_member[CITY_HEADER])):
    print("", file=sys.stderr)
    print(a_member, file=sys.stderr)
    # case : postcode and name match exactly between input and osm
    the_input_postcode = normalize_a_postcode(a_member[POSTAL_CODE_HEADER])
    the_input_name = normalize_a_text(a_member[CITY_HEADER])
    
    if not bool(the_input_postcode) and not bool(the_input_name):
        continue
        
    # case : mistake : the name does not belong to the corpus -> correct it, with the closest word in the corpus / thanks to the postcode
    for an_input_word in the_input_name.split():
        if an_input_word not in the_cities_as_osm_ids__by_normalized_words.keys():
            the_potential_cities_as_osm_ids__according_to_the_postcode = the_cities_as_osm_ids__by_normalized_postcode.get(the_input_postcode, set())
            for a_city_as_osm_id in the_potential_cities_as_osm_ids__according_to_the_postcode:
                for an_osm_name in the_cities_osm__by_osm_id[a_city_as_osm_id]["THE_NAMES"]:
                    for a_word_from_osm in an_osm_name.split():
                        the_distance = levenshtein_distance(an_input_word, a_word_from_osm, 5)
                        #print(an_input_word, a_word_from_osm, the_distance, file=sys.stderr) 
    
    the_potential_cities_as_osm_ids__according_to_the_postcode = the_cities_as_osm_ids__by_normalized_postcode.get(the_input_postcode, set())
    the_potential_cities_as_osm_ids__according_to_the_input_name = the_cities_as_osm_ids__by_normalized_name.get(the_input_name, set())
    the_potential_cities_as_osm_ids = the_potential_cities_as_osm_ids__according_to_the_postcode & the_potential_cities_as_osm_ids__according_to_the_input_name
    
    if len(the_potential_cities_as_osm_ids) == 1: # we found it !
        a_member["the_osm_city"] = the_cities_osm__by_osm_id[tuple(the_potential_cities_as_osm_ids)[0]]
        print("1- ", a_member["the_osm_city"], file=sys.stderr)
        continue
    
    # case : name ok but no postcode in osm
    #the_potential_cities_as_osm_ids = the_potential_cities_as_osm_ids__according_to_the_input_name & the_cities_as_osm_ids__by_normalized_postcode[""]
    #if len(the_potential_cities_as_osm_ids) == 1: # we found it !
    #    a_member["the_osm_city"] = the_cities_osm__by_osm_id[tuple(the_potential_cities_as_osm_ids)[0]]
    #    print("2- ", a_member["the_osm_city"], file=sys.stderr)
    #    continue
    
    
    # case : Vern sur Seiche <-> Vern or St Julien <-> Saint Julien : one word bring to the whole name ; postcode match
    the_potential_cities_as_osm_ids__method_3 = set()
    for an_input_word in the_input_name.split():
        the_potential_cities_as_osm_ids__method_3 |= the_cities_as_osm_ids__by_normalized_words.get(an_input_word, set()) & the_cities_as_osm_ids__by_normalized_postcode.get(the_input_postcode, set())
    if len(the_potential_cities_as_osm_ids__method_3) == 1: # we found it !
        a_member["the_osm_city"] = the_cities_osm__by_osm_id[tuple(the_potential_cities_as_osm_ids__method_3)[0]]
        print("3- ", a_member["the_osm_city"], file=sys.stderr)
        continue
    
    # case : name ok but postcode is not reliable (postcode is not in osm) and several candidates : take the most popular place = the one with so many tags
    the_potential_cities_as_osm_ids = the_potential_cities_as_osm_ids__according_to_the_input_name
    try:
        the_potential_city_as_osm_id = max(the_potential_cities_as_osm_ids, key = lambda an_osm_id : len(the_cities_osm__by_osm_id[an_osm_id]))
        a_member["the_osm_city"] = the_cities_osm__by_osm_id[the_potential_city_as_osm_id]
        print("4- ", a_member["the_osm_city"], file=sys.stderr)
        continue
    except ValueError:
        pass
    
    # case : Vern sur Seiche <-> Vern or St Julien <-> Saint Julien : one word bring to the whole name ; no postcode
    #the_potential_cities_as_osm_ids__method_5 = set()
    #for an_input_word in the_input_name.split():
    #    if an_input_word not in the_meaningless_words:
    #        the_potential_cities_as_osm_ids__method_5 |= the_cities_as_osm_ids__by_normalized_words.get(an_input_word, set())
    #if len(the_potential_cities_as_osm_ids__method_5) == 1: # we found it !
    #    a_member["the_osm_city"] = the_cities_osm__by_osm_id[tuple(the_potential_cities_as_osm_ids__method_5)[0]]
    #    print("5- ", a_member["the_osm_city"], file=sys.stderr)
    #    continue
    
    # else case : 
    #a_member["the_osm_city"] = the_cities_osm__by_osm_id[  min(the_potential_cities_as_osm_ids__method_5, key = lambda a_potential_city_as_osm_id : levenshtein_distance(the_input_name, the_cities_osm__by_osm_id[a_potential_city_as_osm_id]['THE_NAMES'][0], 99))  ]
    #print("6- ", a_member["the_osm_city"], file=sys.stderr)
    
    if bool(the_potential_cities_as_osm_ids):
        a_member["the_osm_city"] = the_cities_osm__by_osm_id[  min(the_potential_cities_as_osm_ids, key = lambda a_potential_city_as_osm_id : levenshtein_distance(the_input_name, the_cities_osm__by_osm_id[a_potential_city_as_osm_id]['THE_NAMES'][0], 99))  ]
        print("6- ", a_member["the_osm_city"], file=sys.stderr)


fieldnames = [NAME_HEADER, POSTAL_CODE_HEADER, CITY_HEADER, 'lon', 'lat']
writer = csv.DictWriter(sys.stdout, fieldnames=fieldnames)
    
writer.writeheader()
for a_member in the_members:
    try:
        writer.writerow({NAME_HEADER : a_member[NAME_HEADER], POSTAL_CODE_HEADER : a_member[POSTAL_CODE_HEADER], CITY_HEADER : a_member[CITY_HEADER], 'lon' : a_member['the_osm_city']['longitude'], 'lat' : a_member['the_osm_city']['latitude']})
    except KeyError: # no osm city
        writer.writerow({NAME_HEADER : a_member[NAME_HEADER], POSTAL_CODE_HEADER : a_member[POSTAL_CODE_HEADER], CITY_HEADER : a_member[CITY_HEADER]})
