city name + zipcode ------> gps coordinates, thanks to openstreetmap data


 % brew install osmium-tool
 % brew install python

 % pip3 install --user pipenv

 % python3 -m site --user-base
/Users/user/Library/Python/3.7

 % cd geoloch
 % /Users/user/Library/Python/3.7/bin/pipenv install osmium

 % /Users/user/Library/Python/3.7/bin/pipenv shell
(geoloch)  % python3
Python 3.7.3 (default, Oct 11 2019, 19:39:43) 
[Clang 11.0.0 (clang-1100.0.33.12)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import osmium
>>> # no ModuleNotFoundError

(geoloch) % cat file.csv | python3 geoloch.py 

(geoloch) % exit
 % 




$ osmium tags-filter -o zipcodes.osm.pbf planet-latest.osm.pbf nwr/addr:postcode --verbose
[ 0:00] Started osmium tags-filter
[ 0:00]   osmium version 1.10.0
[ 0:00]   libosmium version 2.15.0
[ 0:00] Command line options and default settings:
[ 0:00]   input options:
[ 0:00]     file name: planet-latest.osm.pbf
[ 0:00]     file format: 
[ 0:00]   output options:
[ 0:00]     file name: zipcodes.osm.pbf
[ 0:00]     file format: 
[ 0:00]     generator: osmium/1.10.0
[ 0:00]     overwrite: no
[ 0:00]     fsync: no
[ 0:00]   other options:
[ 0:00]     add referenced objects: yes
[ 0:00]   looking for tags...
[ 0:00]     on nodes: yes
[ 0:00]     on ways: yes
[ 0:00]     on relations: yes
[ 0:00] Following references...
[ 0:00]   Reading input file to find relations in relations...
[ 9:29]   Reading input file to find nodes/ways in relations...
[19:14]   Reading input file to find nodes in ways...
[28:52] Done following references.
[28:52] Opening input file...
[28:52] Opening output file...
[28:52] Copying matching objects to output file...
[=====================================>                                ]  53% 
[======================================================================] 100% 
[42:33] Closing output file...
[42:33] Closing input file...
[42:33] Needed 4 passes through the input file.
[42:33] Done.

$ osmium tags-filter zipcodes.osm.pbf -o zipcodes2.osm n/place --verbose
[ 0:00] Started osmium tags-filter
[ 0:00]   osmium version 1.10.0
[ 0:00]   libosmium version 2.15.0
[ 0:00] Command line options and default settings:
[ 0:00]   input options:
[ 0:00]     file name: zipcodes.osm.pbf
[ 0:00]     file format: 
[ 0:00]   output options:
[ 0:00]     file name: zipcodes2.osm
[ 0:00]     file format: 
[ 0:00]     generator: osmium/1.10.0
[ 0:00]     overwrite: no
[ 0:00]     fsync: no
[ 0:00]   other options:
[ 0:00]     add referenced objects: yes
[ 0:00]   looking for tags...
[ 0:00]     on nodes: yes
[ 0:00]     on ways: no
[ 0:00]     on relations: no
[ 0:00] Following references...
[ 0:00] Done following references.
[ 0:00] Opening input file...
[ 0:00] Opening output file...
[ 0:01] Copying matching objects to output file...
[======================================================================] 100% 
[ 0:34] Closing output file...
[ 0:34] Closing input file...
[ 0:34] Needed 1 passes through the input file.
[ 0:34] Done.

$ osmium tags-filter planet-latest.osm.pbf -o places.osm.pbf n/place=city,town,village  --verbose
[ 0:00] Started osmium tags-filter
[ 0:00]   osmium version 1.10.0
[ 0:00]   libosmium version 2.15.0
[ 0:00] Command line options and default settings:
[ 0:00]   input options:
[ 0:00]     file name: planet-latest.osm.pbf
[ 0:00]     file format: 
[ 0:00]   output options:
[ 0:00]     file name: places.osm.pbf
[ 0:00]     file format: 
[ 0:00]     generator: osmium/1.10.0
[ 0:00]     overwrite: no
[ 0:00]     fsync: no
[ 0:00]   other options:
[ 0:00]     add referenced objects: yes
[ 0:00]   looking for tags...
[ 0:00]     on nodes: yes
[ 0:00]     on ways: no
[ 0:00]     on relations: no
[ 0:00] Following references...
[ 0:00] Done following references.
[ 0:00] Opening input file...
[ 0:00] Opening output file...
[ 0:00] Copying matching objects to output file...
[======================================================================] 100% 
[10:01] Closing output file...
[10:01] Closing input file...
[10:01] Needed 1 passes through the input file.
[10:01] Done.


$ osmium tags-filter -o zipcodes_areas.osm.pbf zipcodes.osm.pbf r/type=boundary --verbose
[ 0:00] Started osmium tags-filter
[ 0:00]   osmium version 1.10.0
[ 0:00]   libosmium version 2.15.0
[ 0:00] Command line options and default settings:
[ 0:00]   input options:
[ 0:00]     file name: zipcodes.osm.pbf
[ 0:00]     file format: 
[ 0:00]   output options:
[ 0:00]     file name: zipcodes_areas.osm.pbf
[ 0:00]     file format: 
[ 0:00]     generator: osmium/1.10.0
[ 0:00]     overwrite: no
[ 0:00]     fsync: no
[ 0:00]   other options:
[ 0:00]     add referenced objects: yes
[ 0:00]   looking for tags...
[ 0:00]     on nodes: no
[ 0:00]     on ways: no
[ 0:00]     on relations: yes
[ 0:00] Following references...
[ 0:00]   Reading input file to find relations in relations...
[ 0:30]   Reading input file to find nodes/ways in relations...
[ 1:00]   Reading input file to find nodes in ways...
[ 1:29] Done following references.
[ 1:29] Opening input file...
[ 1:29] Opening output file...
[ 1:30] Copying matching objects to output file...
[======================================================================] 100% 
[ 2:08] Closing output file...
[ 2:08] Closing input file...
[ 2:08] Needed 4 passes through the input file.
[ 2:08] Done.
